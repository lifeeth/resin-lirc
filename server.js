var tty = require('tty.js');

var spawn = require('child_process').spawn;

// Simple tty.js in app mode
var app = tty.createServer({
  shell: 'bash',
  users: {
    admin: 'admin'
  },
  port: process.env.PORT
});

var child = spawn(
	'/etc/init.d/lirc',
	['start'],
	{
	    detached: true,
	    stdio: [ 'ignore', 'ignore', 'ignore' ]
	}
	);


app.get('/toggle', function (req, res, next) {
    res.send('Toggled');
    spawn('/usr/bin/irsend',
	['SEND_ONCE','PowerSwitch', 'KEY_POWER'],
	{
	    detached: true,
	   stdio: [ 'ignore', 'ignore', 'ignore' ]
	}
	);
});


app.listen();
